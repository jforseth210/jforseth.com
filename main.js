$(document).ready(function () {
  // Check for click events on the navbar burger icon
  $(".navbar-burger").click(function () {
    // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
    $(".navbar-burger").toggleClass("is-active");
    $(".navbar-menu").toggleClass("is-active");
  });
});


const videoCardsContainer = document.getElementById('video-cards');
let videos = [];
let currentNumberOfVideos = 0;
let incrementVideosBy = 3;
(async () => {
  const request = await fetch("videos.json");
  videos = await request.json();
  showMore(false);
  showMore(false);
})(
);

function showMore(shouldScroll = true) {
  for (let i = currentNumberOfVideos; i < currentNumberOfVideos + incrementVideosBy; i++) {
    const card = createVideoCard(videos[i])
    videoCardsContainer.appendChild(card);
    setTimeout(() => {
      console.log(videoCardsContainer.children[i]);
      videoCardsContainer.children[i].style.maxHeight = '1000px';
    }, 10);
    setTimeout(() => {
      if (shouldScroll) {
        videoCardsContainer.children[i].scrollIntoView();
      }
    }, 150);
  }
  currentNumberOfVideos += incrementVideosBy;
}

// Function to create a video card using a template
function createVideoCard(video) {
  const template = document.getElementById('video-card-template');
  const clone = document.importNode(template.content, true);

  const link = clone.querySelector('a');
  link.href = `https://youtube.com/v/${video.video_id}`;

  const image = clone.querySelector('img');
  image.src = `https://img.youtube.com/vi/${video.video_id}/maxresdefault.jpg`;
  image.alt = video.title;

  const title = clone.querySelector('.title');
  title.textContent = video.title;

  return clone;
}